//
//  ViewController.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 25/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    
    
    //Array with all shots
    private var data = [Shots]()
    //service URL to get shots
    private let serviceURL = "http://api.dribbble.com/v1/shots"
    //Max shots capacity
    private let shotsCap = 30
    
    
    
    private var selectedShot : Shots!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 360.0;
        

        
        RestManager.sharedInstance.getShots(path: serviceURL, completion: {(result: NSArray) in
            
            //Iterate through results to add shots to our data array
            for shot in result {
                //If we reached max shots capacity stop adding more of them
                if(self.data.count == self.shotsCap){
                    return;
                }
                
                //add shot to data array
                self.data.append(Shots.init(dataDict: shot as! NSDictionary))
                
            }
            
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table View functions
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainCell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as! MainTableViewCell
        
        cell.populateCell(shot: data[indexPath.row])
        
        cell.updateConstraintsIfNeeded()
        
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedShot = data[indexPath.row]
        
        self.performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    // MARK: segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetail") {
            
            let svc = segue.destination as! ShotDetailViewController;
            
            svc.shotToShow = self.selectedShot
            
        }
    }
    
    
}

