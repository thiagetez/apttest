//
//  ShotsDetailTableViewCell.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 27/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import UIKit

class ShotsDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shotImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func inflateCell(shotToShow : Shots!){
        self.titleLabel.text = shotToShow.title
        self.shotImageView.loadImageFromURL(imageURL: shotToShow.imageURL)
        
        //Sometimes Shots doesn't have description
        if(shotToShow.description != nil){
            self.descLabel.text = shotToShow.description
        }else{
            self.descLabel.text = "Sem descrição para este item."
        }
        

        
        self.viewsLabel.text = "Views: \(String(shotToShow.viewsCount))"
        self.commentsLabel.text = "Comments: \(String(shotToShow.commentsCount))"
        self.createdLabel.text = "Created at: \(shotToShow.createdAt.formatStringDate())"
        
    }

}
