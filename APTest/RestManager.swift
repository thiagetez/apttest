//
//  RestManager.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 25/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import Foundation

//This class will handle all requests to the endpoint
class RestManager : NSObject{
    
    
    
    static let sharedInstance = RestManager()
    //I should declare token in a config class, but since this will only make the project more complex
    //i will declare it here
    let token = "46cc5347337e4151e061af2e4b90642b0c4395d9641f0d135519d9320a10ee77"
    
    //Rough request to get Shots
    public func getShots(path: String, completion:@escaping (NSArray)->()){
        let request : NSMutableURLRequest = NSMutableURLRequest()
        
        let url = path.appending("?access_token=\(token)")
        request.url = NSURL(string: url) as URL?
        request.httpMethod = "GET"
    
        let session = URLSession.shared
        
        
        session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            do {
                let jsonResult: NSArray! = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSArray
                if (jsonResult != nil) {
                    // process jsonResult
                    print("Could get Shots")
                    print(jsonResult)
                    completion(jsonResult)
                } else {
                    print("No Shots - ERROR:")
                    print(error)
                    // couldn't load JSON, look at error
                }
            }
            catch {
                print("Error Occured")
            }
        }.resume()
    }
    
}
