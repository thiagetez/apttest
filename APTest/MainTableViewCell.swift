//
//  MainTableViewCell.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 25/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var teaserImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func populateCell(shot : Shots){
        self.titleLabel.text = shot.title;
        
        if(shot.description != nil){
            self.descLabel.text = shot.description
        }else{
            self.descLabel.text = "Sem descrição para este item."
        }
        
        //Load image Async
        //I've created a UImageView Extension
        //So it's easy to load a shot image from url through the project and
        //prevent code duplication
        self.teaserImageView.loadImageFromURL(imageURL: shot.teaserURL)
        
        self.viewsCountLabel.text = "Views: \(String(shot.viewsCount))"
        self.commentCountLabel.text =  "Comments: \(String(shot.commentsCount))"
        let date = shot.createdAt.formatStringDate()
        self.createdAtLabel.text = "Created at: \(date)"
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
