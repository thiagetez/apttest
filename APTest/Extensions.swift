//
//  Extensions.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 27/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import UIKit

//UIImageView Extension to prevent code dup
//And make easyly
extension UIImageView{
    
    func loadImageFromURL(imageURL : String!){
        let url = URL(string: imageURL)
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if(data != nil){
                    self.image = UIImage(data: data!)
                }
                
            }
        }
    }
}

extension String{
    
    func formatStringDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        dateFormatter.timeZone = NSTimeZone.local as TimeZone!
        
        guard let newDate = dateFormatter.date(from: self) else {
            assert(false, "no date from string")
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"
        dateFormatter.timeZone = NSTimeZone.local as TimeZone!
        let timeStamp = dateFormatter.string(from: newDate)
        
        return timeStamp
    }
}
