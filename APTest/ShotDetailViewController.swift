//
//  ShotDetailViewController.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 27/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import UIKit

class ShotDetailViewController: UITableViewController {
    
    
    public var shotToShow : Shots!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        
        tableView.estimatedRowHeight = 350.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.title = "Shot Detail"
        self.navigationItem.backBarButtonItem?.title = "voltar"
        
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table View functions
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shotDetailCell", for: indexPath) as! ShotsDetailTableViewCell
        
        cell.inflateCell(shotToShow: shotToShow)
        
        cell.updateConstraintsIfNeeded()
        
        
        return cell
        
    }
    
    
    
    
    
    
}
