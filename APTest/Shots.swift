//
//  Shots.swift
//  APTest
//
//  Created by Thiago Pontes Lima on 25/11/16.
//  Copyright © 2016 Thiago Pontes Lima. All rights reserved.
//

import Foundation


class Shots {
    
    let nId : Int
    let title : String
    let imageURL : String
    let teaserURL : String
    let description : String?
    let viewsCount : Int
    let commentsCount : Int
    let createdAt : String
    
    init(dataDict: NSDictionary) {
        self.nId = dataDict["id"] as! Int
        self.title = dataDict["title"] as! String
        let imageDict = dataDict["images"] as! NSDictionary
        self.teaserURL = imageDict["teaser"] as! String
        self.imageURL = imageDict["normal"] as! String
        self.description = dataDict["description"] as? String
        self.viewsCount  = dataDict["views_count"] as! Int
        self.commentsCount = dataDict["comments_count"] as! Int
        self.createdAt = dataDict["created_at"] as! String
    }
    
    
    
}
